# Dissertation-Cloud-Detection

This repository contains the various codes for my Masters' Dissertation project, Machine Learning for Image Classification over Polar Regions.

A brief overview of the files: 

- Clustering directory (K-Means, GMM, Fuzzy C-Means, GMM with multiple scenes, Semi-supervised K-means and GMM)
- Min-max directory for minimum and maximum brightness temperatures and radiance values
- Visualisation directory that contains various preprocessing plots/graphs, confusion matrices
